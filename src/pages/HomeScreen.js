import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'contact.db' });

export default class HomeScreen extends Component {
    constructor(props) {
        super(props)
        db.transaction(function (txn) {
            txn.executeSql("SELECT * from sqlite_master WHERE type='table' AND name='contactv2'",[],
                function (tx, res) {
                    if (res.row.length == 0) {
                        txn.executeSql('DROP TABLE IF EXIST contactv2', [])
                        txn.executeSql('CREATE TABLE IF NOT EXISTS contactv2(id VARCHAR(30) PRIMARY KEY AUTOINCREMENT, name VARCHAR(50), phone_number VARCHAR(11), avatar VARCHAR(MAX))', []);
                    }
                }
            )
        })
    }


    state = {
        user: [
            {
                id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
                title: 'Mai Đức Thắng',
                avatar: 'http://pluspng.com/img-png/user-png-icon-account-avatar-human-male-man-men-people-person-download-svg-download-png-edit-icon-512.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
            {
                id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
                title: 'Võ Hồng Phong',
                avatar: 'http://pluspng.com/img-png/user-png-icon-account-avatar-human-male-man-men-people-person-download-svg-download-png-edit-icon-512.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
            {
                id: '58694a0f-3da1-471f-bdfrfr96-145571e29d72',
                title: 'Dương Viết Minh Trí',
                avatar: 'https://img.icons8.com/clouds/2x/user.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
            {
                id: 'bd7acbea-c1b1-46c2-arfred5-3ad53abb28ba',
                title: 'Mai Đức Thắng',
                avatar: 'http://pluspng.com/img-png/user-png-icon-account-avatar-human-male-man-men-people-person-download-svg-download-png-edit-icon-512.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
            {
                id: '3ac68afc-c605-48d3-a4dddf8-fbd91aa97f63',
                title: 'Võ Hồng Phong',
                avatar: 'http://pluspng.com/img-png/user-png-icon-account-avatar-human-male-man-men-people-person-download-svg-download-png-edit-icon-512.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
            {
                id: '58694a0f-3da1-471f-frbd96-145571e29d72',
                title: 'Dương Viết Minh Trí',
                avatar: 'https://img.icons8.com/clouds/2x/user.png',
                contact: 'https://blog.eshop-gyorsan.hu/wp-content/uploads/2013/06/telefon_ikon.png',
                phone_number: '0999999999'
            },
        ]
    }

    AddUser = () => {
        this.props.navigation.navigate('AddScreen')
    }

    EditUser = (user) => {
        this.props.navigation.navigate('EditScreen', {
            user: user,
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.searchbar}>
                    <View style={styles.boxsearchIcon}>
                        <Image
                            style={styles.searchIcon}
                            source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/search.png')}>
                        </Image>
                    </View>
                    <View style={styles.boxsearch}>
                        <TextInput style={styles.textSearch}
                            underlineColorAndroid='transparent'
                            placeholder='Search here...'
                        ></TextInput>
                    </View>
                </View>
                <View style={styles.listuser}>
                    <FlatList
                        data={this.state.user}
                        renderItem={({ item }) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => this.EditUser(item)}
                                >
                                    <View style={styles.item}>

                                        <Image style={styles.image}
                                            source={{ uri: item.avatar }}>
                                        </Image>
                                        <Text style={styles.info}>{item.title}</Text>
                                        <Image style={styles.image}
                                            source={{ uri: item.contact }}>
                                        </Image>
                                    </View>
                                </TouchableOpacity>

                            )
                        }}
                        keyExtractor={item => item.id}
                    />
                </View>
                <View style={styles.addUser}>
                    <TouchableOpacity
                        onPress={this.AddUser}
                    >
                        <Image style={styles.addUserButton} source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/plus.png')}>

                        </Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        margin: '4%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1,
    },
    searchbar: {
        flexDirection: 'row',
        flex: 10,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 50,
        paddingLeft: '5%',
        paddingRight: '5%',
    },
    listuser: {
        marginTop: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 90,
        width: '100%',
    },
    boxsearchIcon: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        flex: 1,
    },
    searchIcon: {
        resizeMode: 'stretch',
        alignItems: 'center',
        width: '95%',
        height: '60%'
    },

    boxsearch: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flex: 9,
        paddingLeft: '3%'
    },

    textSearch: {
        fontSize: 20
    },
    item: {
        flexDirection: 'row',
        padding: 10,
        height: 60,
        width: '91.5%',
        backgroundColor: '#ffffff',
        borderRadius: 5,
        margin: 7.5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 0.1
    },
    image: {
        height: 45,
        width: 45,
        padding: 0,
        resizeMode: 'cover',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
        borderRadius: 50
    },
    info: {
        fontSize: 18
    },
    addUser: {
        flex: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%',
        marginRight: '3%'
    },

    addUserButton: {
        width: 50,
        height: 50
    }
})


import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, TextInput } from 'react-native';

styles2 = StyleSheet.create({
    container: {
        alignItems: 'center',
        margin: '3%'
    },
    avatarBox: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '40%'
    },
    infoBox: {
        height: '60%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    name: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        borderWidth: 0.1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        height: '20%',
        marginTop: '5%',
        marginBottom: '5%',
        paddingLeft: '5%'
    },
    image: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        height: 150,
        width: 150,
        borderRadius: 75,
        borderWidth: 1
    },
    icon: {
        height: 30,
        width: 30,
        borderRadius: 15,
        paddingLeft: '3%',
    },
    textInfo: {
        fontSize: 20,
        paddingLeft: '5%'
    }

})

export default class EditScreen extends Component {
    constructor(props) {
        super(props);
        params = props.navigation.state.params.user;
        navigation = this.props.navigation
    }

    state = {
        username: this.props.navigation.state.params.user.title,
        phone_number: this.props.navigation.state.params.user.phone_number
    }

    static navigationOptions = {
        headerTitleStyle: { alignSelf: 'center' },
        headerTitle: 'Edit',
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('HomeScreen')}
            ><Image
                style={styles2.icon}
                source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/success.png')}></Image></TouchableOpacity>
        ),
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('HomeScreen')}
            ><Image
                style={styles2.icon}
                source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/error.png')}></Image></TouchableOpacity>
        ),
    };

    render() {
        return (
            <View style={styles2.container}>
                <View style={styles2.avatarBox}>
                    <View style={styles2.image}>
                        <Image
                            style={styles2.avatar}
                            source={{ uri: 'https://www.nationalgeographic.com/content/dam/animals/thumbs/rights-exempt/mammals/d/domestic-dog_thumb.jpg' }}
                        >
                        </Image>
                    </View>
                </View>
                <View style={styles2.infoBox}>
                    <View style={styles2.name}>
                        <Image
                            style={styles2.icon}
                            source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/name.png')}></Image>
                        <TextInput
                            underlineColorAndroid='transparent'
                            placeholder='Add your name here...'
                            style={styles2.textInfo}
                        >
                            {this.state.username}
                        </TextInput>
                    </View>
                    <View style={styles2.name}>
                        <Image
                            style={styles2.icon}
                            source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/contact.png')}></Image>

                        <TextInput
                            underlineColorAndroid='transparent'
                            placeholder='Add your phone number here...'
                            style={styles2.textInfo}
                        >
                            {this.state.phone_number}
                        </TextInput>
                    </View>
                </View>
            </View>
        )
    }
}
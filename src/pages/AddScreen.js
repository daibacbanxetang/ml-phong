import React, { Component } from 'react';
import { View, TouchableOpacity, Image, TextInput, StyleSheet } from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'contact.db' });

styles1 = StyleSheet.create({
    container: {
        alignItems: 'center',
        margin: '3%'
    },
    avatarBox: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '40%'
    },
    infoBox: {
        height: '60%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    name: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        borderWidth: 0.1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        height: '20%',
        marginTop: '5%',
        marginBottom: '5%',
        paddingLeft: '5%'
    },
    image: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        height: 150,
        width: 150,
        borderRadius: 75,
        borderWidth: 1
    },
    icon: {
        height: 30,
        width: 30,
        borderRadius: 15,
        paddingLeft: '3%',
    },
    textInfo: {
        fontSize: 20,
        paddingLeft: '5%'
    }

})

export default class AddScreen extends Component {
    constructor(props) {
        super(props);
        navigation = this.props.navigation
        this.state = {
            name: '',
            phone_number: '',
            avatar: '',
        };

    }

    addContact = (name, phone_number) => {
        if (name) {
            if (phone_number) {
                db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO contactv2 (name, phone_number, avatar) VALUES (?,?,?)', [name, phone_number, 'https://www.nationalgeographic.com/content/dam/animals/thumbs/rights-exempt/mammals/d/domestic-dog_thumb.jpg'], (tx, results) => {
                    if (results.rowsAffected > 0) {
                            Alert.alert(
                                'Success',
                                'You are Registered Successfully',
                            );
                        }
                        else {
                            alert('Registration Failed');
                        }
                    })
                })
            }
        }
    }
    componentDidMount() {
        this.props.navigation.setParams({ addContact: this.addContact });
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitleStyle: { alignSelf: 'center' },
        headerTitle: 'Add',
        headerRight: (
            <TouchableOpacity
                onPress={() => { navigation.getParam('addContact')('Mai Đức Thắng', '0388810584') }}
            ><Image
                style={styles1.icon}
                source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/success.png')}></Image></TouchableOpacity>
        ),
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('HomeScreen')}
            ><Image
                style={styles1.icon}
                source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/error.png')}></Image></TouchableOpacity>
        ),
    });
    render() {
        return (
            <View style={styles1.container}>
                <View style={styles1.avatarBox}>
                    <View style={styles1.image}>
                        <Image
                            style={styles1.avatar}
                            source={{ uri: 'https://www.nationalgeographic.com/content/dam/animals/thumbs/rights-exempt/mammals/d/domestic-dog_thumb.jpg' }}
                        >
                        </Image>
                    </View>
                </View>
                <View style={styles1.infoBox}>
                    <View style={styles1.name}>
                        <Image
                            style={styles1.icon}
                            source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/name.png')}></Image>
                        <TextInput
                            underlineColorAndroid='transparent'
                            placeholder='Add your name here...'
                            style={styles1.textInfo}
                        >{this.state.name}</TextInput>
                    </View>
                    <View style={styles1.name}>
                        <Image
                            style={styles1.icon}
                            source={require('/home/maithang/Duc Thang/DUT/React Native/contactv2/images/contact.png')}></Image>

                        <TextInput
                            underlineColorAndroid='transparent'
                            placeholder='Add your phone number here...'
                            style={styles1.textInfo}
                        >
                            {this.state.phone_number}</TextInput>
                    </View>
                </View>
            </View>
        )
    }
}
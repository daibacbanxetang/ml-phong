import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/pages/HomeScreen'
import AddScreen from './src/pages/AddScreen'
import EditScreen from './src/pages/EditScreen'





/**************** Add User Screen ***************/


/**************** Edit User Screen ***************/



const App = createStackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            header: null,
        },
    },
    AddScreen: {
        screen: AddScreen,
        
    },
    EditScreen: {
        screen: EditScreen,
    },
},
{headerLayoutPreset: 'center'})

export default createAppContainer(App)